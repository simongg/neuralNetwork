from random import randint

'''
FUNCTIONS
'''

#Math Functions
def dist(network, i, j):
	'''
	Calculates the distance between two nodes
	PRIMATIVE
	'''
	if network[i][j] == True:
		return 1
	else:
		return 0

#Network Construction Functions
def create_delays(network, delay_min, delay_max):
	'''
	Creates a N by N matrix of random delays
	'''
	N = len(network)
	delay = []
	for n_out in range(N):
		delay.append( [] )
		
		for n_in in range(N):
			if network[n_out][n_in] == True:
				delay[n_out].append( randint(delay_min, delay_max) + dist(network, n_out, n_in) - 1)
			else:
				delay[n_out].append( 0 )
	return delay

def create_nodes(network, delays):
	'''
	Creates list of nodes
	'''
	N = len(network)
	node = []
	for n in range(N):
		node.append( Node(network, delays, n) )
	return node

# Class Construction Functions
def construct_input_list(self):
	'''
	Creates list of nodes that the node accepts inputs from
	'''
	input_list = []
	for n in range(self.N):
		if network[n][self.index] == True:
			input_list.append(n)
	return input_list

def construct_input_hold(self):
	'''
	Creates a list to store inputs in
	'''
	input_hold = []
	for i in range( self.input_length ):
		input_hold.append(False)
	return input_hold

def construct_delay_hold(self):
	'''
	Creates the delay mechanism
	'''
	delay_hold = []
	for i in range( self.input_length ):
		delay_hold.append( [] )
		for d in range( max( self.delays[ self.input_list[i] ] ) ):
			delay_hold[i].append(False)
	return delay_hold

def construct_delay_list(self):
	'''
	Finds the amount of delay from inputs
	'''
	delay_list = []
	for i in self.input_list:
		delay_list.append( self.delays[i][self.index] )
	return delay_list

def collect_garbage(self):
	'''
	Deletes stuff that will no longer be used
	'''
	del(self.network)
	del(self.delays)


'''
CLASSES
'''

class Node(object):
	
	def __init__(self, network, delays, index, excitatory = True):
	
		self.network = network
		self.delays = delays
		self.N = len(self.network)
		
		self.index = index
		self.excitatory = excitatory
		
		self.input_list = construct_input_list(self)
		self.input_length = len(self.input_list) 
		self.input_hold = construct_input_hold(self)
		self.delay_list = construct_delay_list(self)
		self.delay_hold = construct_delay_hold(self)
		self.output_hold = False

		collect_garbage(self)

	def grab_input_from(self, node):
		for i in range( self.input_length ):
			incoming_from = self.input_list[i]
			self.input_hold[i] = node[incoming_from].output_hold

	def input_to_delay(self, t):
		for i in range( self.input_length ):
			location = t % self.delay_list[i]
			self.delay_hold[i][location] = self.input_hold[i]

	def to_output(self, t):
		total = 0
		for i in range( self.input_length ):
			location = t % self.delay_list[i]
			if self.delay_hold[i][location] == True:
				total += 1
		if total >= 2:
			self.output_hold = True
		else:
			self.output_hold = False

	def fire(self):
		self.output_hold = True


'''
MAIN
'''
N = 5
dimension = 1
delay_lower = 2
delay_upper = 4
threshold = 10
syn_init = 5
simL = 60000

network = 	[ 	[False,	True, 	False, 	False, 	True],
				[True, 	False, 	True, 	False, 	False],
				[False, True, 	False, 	True, 	False],
				[False, False, 	True, 	False, 	True],
				[True, 	False, 	False, 	True, 	False] 	]

delays = create_delays(network, delay_lower, delay_upper)

#construct CLASSES
node = create_nodes(network, delays)
node[0].fire()
node[2].fire()
node[4].fire()


print("START")
for t in range( simL ):					#Run for simL milliseconds
    for n in range( len(node) ):		#for each node
        node[n].grab_input_from(node)	#grab the output from the appropriate nodes and add to current node's input
    for n in range( len(node) ):		#for each neuron
        node[n].to_output(t)			#take any inputs that have arrived at the same time and see if they cause the current node to spike
    for n in range( len(node) ):		#for each neuron
        node[n].input_to_delay(t)		#load delay
    print node[n].output_hold
print("DONE")

