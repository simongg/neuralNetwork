import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import random

#################
# this is Simon's attempt at creating a network of 
# neurons, just for fun. Maybe it will be the first
# conscious computerized thing! but I doubts it.

# randomly creates neurons, connects them with a delay, and 
# runs it for a certain time
  
class neuron:

        def __init__(self, a=None, b=None, c=None, d=None):
                if a is None:
                  self.a = random.uniform(0.18, 0.21)
                else:
                  self.a = a
                if b is None:
                  self.b = random.uniform(0.2, 0.3)
                else:
                  self.b = b
                if c is None:
                  self.c = random.randint(-65, -45)
                else:
                  self.c = c
                if d is None:
                  self.d = random.randint(4,7)
                else:
                  self.d = d
                if a is None:
                  self.a = random.uniform(0.18, 0.21)
                else:
                  self.a = a
                
                self.v = self.c
                self.u = self.b*self.v
                self.VRange = []
                
        def __repr__(self):
          return "a="+str(self.a)+" b="+str(self.b)+" c="+str(self.c)+" d="+str(self.d)
          
        def __str__(self):
          return "a="+str(self.a)+" b="+str(self.b)+" c="+str(self.c)+" d="+str(self.d)
        
        def integrate(self, voltageInput):
                
                #derivatives of voltage and membrane recovery variable
                vPrime = (0.04*self.v*self.v + 5*self.v + 140 - self.u + voltageInput)
                uPrime = self.a*(self.b*self.v - self.u)

                #integrate
                self.v += vPrime*0.1
                self.u += uPrime*0.1
                self.VRange.append(self.v)
                
                #if voltage spikes, reset
                if self.v >= 30:
                        Vspike = self.v
                        spike = 1
                        self.v = self.c
                        self.u += self.d
                        return spike, Vspike
                spike = 0
                return spike,self.v
		
	# initialize the actual neurons 
nr = [] # make empty list
nr.append(neuron(0.185, 0.279,-50,5)) # double spike
nr.append(neuron(a=0.19374565370706587, b=0.29983622383541997, c=-45, d=6)) # tripple  spike
nr.append(neuron())
nr.append(neuron())
nr.append(neuron(a=0.1, b=0.2, c=-60, d=5)) 

numberOfNeurons = len(nr)
		# initialize the neuron delay 
neuronDelay = [random.randint(50,100) for i in range(numberOfNeurons)]
print(neuronDelay)

	# initialize the matrix of starting voltages based on the neuron delays
connectMatrixVoltage = [[random.randint(2,5) for j in range(neuronDelay[i])] for i in range(len(neuronDelay))]

    # initialize how long we will run it for
time = 1500

print(connectMatrixVoltage)

for JJ in range(0, time):
  for jj in range(len(nr)):
    #print(j)
    voltageC = connectMatrixVoltage[:] 
    voltageC.pop(jj)
    
    voltage = 0
    for ii in range(len(nr)-1):
      voltage += voltageC[ii][-1]
    voltage = voltage/(len(nr)-1) # get the average value of the incoming spikes
    
    spike, voltage = nr[jj].integrate(voltage)
    for ii in range(random.randint(2,10)):
      connectMatrixVoltage[jj].insert(0,spike*voltage)
  
  for ii in range(len(nr)-1):
    connectMatrixVoltage[ii].pop()

for ii in range(len(nr)):
  print(str(ii+1))
  print(nr[ii])
	
plt.plot(nr[-1].VRange)
print(connectMatrixVoltage[-1])


plt.savefig('graph.png')

plt.plot(nr[0].VRange)
plt.plot(nr[1].VRange)
plt.plot(nr[2].VRange)
plt.legend(("nr1","nr2","nr3"))
plt.savefig('graph2.png')
